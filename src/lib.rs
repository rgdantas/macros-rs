#[macro_export]
macro_rules! expr_token(
    ($expr:expr) => {{ $expr }};
);

#[macro_export]
macro_rules! hashmap {
    ($key:tt : $value:expr) => {
        hashmap! { $key : $value }
    };
    ($key0:tt : $value0:expr $(, $keyN:tt : $valueN:expr)*) => {
        hashmap! { 
            $key0 : $value0,
            $(
                $keyN : $valueN,
            )*
        }
    };
    ($($key:tt : $value:expr,)*) => {{
        let mut map = HashMap::new();
        $(
            map.insert(From::from(expr_token!($key)), From::from($value));
        )*
        map
    }};
}
